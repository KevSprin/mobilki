import React, { Component } from 'react';
import { Text, TextInput, View, Button, Image, Alert, FlatList, StyleSheet, TouchableOpacity  } from 'react-native';


const ListItems = [
  { key: '1', value: 'Pierwszy'},
  { key: '2', value: 'Drugi'},
  { key: '3', value: 'Trzeci'},
  { key: '4', value: 'Czwarty'},
  { key: '5', value: 'Piąty'},
  { key: '6', value: 'Szóśty'},
  { key: '7', value: 'Siódmy'},
  { key: '8', value: 'Ósmy'},
];


function Item({ id, title }) {
  return (
    <TouchableOpacity  onPress={() => { Alert.alert('Key!', JSON.stringify(id) , [{text: 'OK'}],{cancelable: false})}} style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'powderblue',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 12,
  },
});

export default class FlexDimensionsBasics extends Component {
  render() {
    return (

      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <View style={{flex: 1, backgroundColor: 'powderblue', justifyContent: 'center', alignItems: 'center'}}>
          <View style={{width: 150, justifyContent: 'center'}}>
            <Button onPress={() => { Alert.alert('Alert!', 'To jest alert', [{text: 'OK'}, {text: 'Cancel'}],{cancelable: false}); }} title="Press Me" />
          </View>
        </View>
        <View style={{flex: 3, backgroundColor: 'skyblue'}}>
          <Image source={{ uri: 'https://facebook.github.io/react/logo-og.png' }} style={{height: 300}}/>
        </View>
        <View style={{flex: 2, backgroundColor: 'steelblue'}}>
        <FlatList
            data={ListItems}
            renderItem={({ item }) => <Item id={item.key} title={item.value}/>}
            keyExtractor={item => item.key}
          />
        </View>
      </View>
    );
  }
}
