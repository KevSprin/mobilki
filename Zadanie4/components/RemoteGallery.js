/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import LoadingCirle from './LoadingCircle';

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'powderblue',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    alignItems: 'center',
  },
  title: {
    fontSize: 12,
  },
});

export default class RemoteGallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      pictureID: 0,
      pictureURL: 'https://facebook.github.io/react/logo-og.png',
    };
  }

  componentDidMount() {
    return fetch('http://fizyka.umk.pl/~kdobosz/pum/nebula-images.json')
      .then(response => response.json())
      .then(responseJson => {
        this.setState({isLoading: false, dataSource: responseJson.images});
      })
      .catch(error => {
        console.error(error);
      });
  }

  getPictureID(id) {
    if (!this.state.isLoading) {
      let newId = parseInt(id, 10) - 1;
      this.setState({pictureID: newId});
      this.setState({pictureURL: this.state.dataSource[newId].url});
    }
  }

  // Zrobić wyświetlanie zdjęcia
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1}}>
          <LoadingCirle color="yellow" backgroundColor="black" />
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 2}}>
          <Image
            source={{uri: this.state.pictureURL}}
            style={{flex: 1, width: win.width, alignSelf: 'center'}}
          />
        </View>
        <View style={{flex: 1}}>
          <FlatList
            style={{backgroundColor: 'steelblue'}}
            data={this.state.dataSource}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => {
                  this.getPictureID(item.id);
                }}
                style={styles.item}>
                <Text style={styles.title}>{item.title}</Text>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}
