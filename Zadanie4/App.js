/**
 * @format
 * @flow
 */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, TouchableOpacity, Button} from 'react-native';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import RemoteGallery from './components/RemoteGallery';

class HomeScreen extends React.Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'skyblue',
        }}>
        <Text>Home!</Text>
        <Button
          onPress={() => navigate('RemoteGallery')}
          title="Go to Gallery!"
        />
      </View>
    );
  }
}

class RemoteGalleryScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title: 'Remote Gallery',
    headerLeft: (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Button title="Home" onPress={() => navigation.navigate('Home')} />
      </View>
    ),
    headerRight: (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: 10,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Details')}>
          <Text>Details</Text>
        </TouchableOpacity>
      </View>
    ),
  });
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'stretch',
        }}>
        <View style={{flex: 1}}>
          <RemoteGallery />
        </View>
      </View>
    );
  }
}

class RemoteGalleryDetails extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'skyblue',
        }}>
        <Text>Here are some details</Text>
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator({
  Home: HomeNavigation,
  About: AboutScreen,
=======
const RemoteGalleryNavigator = createStackNavigator(
  {
    RemoteGallery: RemoteGalleryScreen,
    Details: RemoteGalleryDetails,
  },
  {
    initialRouteName: 'RemoteGallery',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'steelblue',
      },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        flex: 1,
      },
    },
  },
);
// Zaimplementować przycisk do przekierowania

const HomeNavigator = createDrawerNavigator({
  Home: HomeScreen,
  RemoteGallery: RemoteGalleryNavigator,
});

export default createAppContainer(HomeNavigator);
