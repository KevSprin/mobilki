/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ActivityIndicator} from 'react-native';

export default function LoadingCirle(props) {
  let backgroundColor = 'black';
  let color = 'blue';
  if (props.hasOwnProperty('backgroundColor')) {
    backgroundColor = props.backgroundColor;
  }
  if (props.hasOwnProperty('color')) {
    color = props.color;
  }
  return (
    <ActivityIndicator
      color={color}
      size="large"
      style={{flex: 1, backgroundColor: backgroundColor}}
    />
  );
}
