/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-new-wrappers */
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import {
  accelerometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';

setUpdateIntervalForType(SensorTypes.accelerometer, 10);

const height = Dimensions.get('window').height - 20;
const width = Dimensions.get('window').width - 20;

export default class Ball extends Component {
  constructor(props) {
    super(props);

    this.state = { x: width / 2, y: height / 2, z: 0 };
    this.subscription = null;
  }

  componentWillUnmount(){
    this.subscription.unsubscribe();
    console.log('Leaving Zad7');
  }

  componentDidMount() {
    this.subscription = accelerometer.subscribe(({ x, y, z, timestamp }) => {
      console.log({ x, y, z, timestamp });

      if (
        (this.state.x + x >= width && x > 0) ||
        (this.state.x + x <= 0 && x < 0)
      ) {
        x = 0;
      }
      if (
        (this.state.y + y >= height && y > 0) ||
        (this.state.y + y <= 0 && y < 0)
      ) {
        y = 0;
      }

      this.setState(state => ({
        x: x + state.x,
        y: y + state.y,
        z: z + state.z,
      }));
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{
            width: 20,
            height: 20,
            position: 'absolute',
            top: this.state.y,
            right: this.state.x,
          }}
          source={require('../ball.jpg')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  headline: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
  },
  valueContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  valueValue: {
    width: 200,
    fontSize: 20,
  },
  valueName: {
    width: 50,
    fontSize: 20,
    fontWeight: 'bold',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
