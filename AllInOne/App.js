/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
/* eslint-disable keyword-spacing */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App with Firebase
 * https://github.com/invertase/react-native-firebase
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, Button, StatusBar, Text, TouchableOpacity } from 'react-native';
import SignInScreen, { } from './Components/SignInScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';
import FlexDimensionsBasics from './Components/FlexDimensionBasics';
import Klikacz from './Components/Klikacz';
import Ball from './Components/Ball';
import RemoteGallery from './Components/RemoteGallery';

// Home screen of the app
class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      userInfo : null,
      isLoggedIn : false,
      prev: null,
    };
  }

  static navigationOptions = {
    title: 'AllInOne',
  };

  render() {
    if(this.state.isLoggedIn === true){
      return (
        <View style={styles.container}>
          <View style={styles.container_row1}>
            <View style={{ flex: 1 , backgroundColor: 'lightblue', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="FlexDimensionBasics" onPress={this._showZad1}>
                <Text style={styles.text}>FlexDimensionBasics</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex : 1, backgroundColor: 'royalblue', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="Klikacz" onPress={this._showZad2}>
                <Text style={styles.text}>Klikacz</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.container_row2}>
            <View style={{ flex : 1, backgroundColor: 'dodgerblue', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="Zdalna galeria" onPress={this._showZad3}>
                <Text style={styles.text}>Zdalna galeria</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex : 1, backgroundColor: 'aquamarine', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="Sensory: Akcelerometr" onPress={this._showZad7}>
                <Text style={styles.text}>Sensory: Akcelerometr</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.container_row3}>
            <View style={{ flex : 1, backgroundColor: 'lightsteelblue', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="Profile" onPress={this._profileScreen}>
                <Text style={styles.text}>Profile</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex : 1, backgroundColor: 'steelblue', justifyContent: 'center'}}>
              <TouchableOpacity style={styles.button} title="About" onPress={this._showMoreApp}>
                <Text style={styles.text}>About</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
    else{
      return (
        <View style={styles.container}>
          <Button title="Sign in!" onPress={this._profileScreen} />
        </View>
      );
    }
  }

  componentDidMount(){
    console.log('Trying to retrieve data..');
    this.retrieveData();
  }

  componentDidUpdate(){
    this.retrieveData();
  }

  retrieveData = async() => {
    try{
      const info = await AsyncStorage.getItem('userInfo');
      if(this.state.prev !== info) {
        this.setState({userInfo : info});
        if(this.state.isLoggedIn === false && info !== null){
          console.log('Data retrieved!');
          this.setState({isLoggedIn : true});
        }
        else if(info === null && this.state.isLoggedIn === true && this.state.userInfo === null){
          console.log('No data retrieved or logged off');
          this.setState({isLoggedIn : false});
        }
      }
      this.setState({prev: info});
    }catch(error){
      console.log(' Error Message: ', error.message);
    }
  }

  _showZad1 = () => {
    console.log('Navigating to Zad1');
    this.props.navigation.navigate('Zad1');
  }

  _showZad2 = () => {
    console.log('Navigating to Zad2');
    this.props.navigation.navigate('Zad2');
  }

  _showZad3 = () => {
    console.log('Navigating to Zad3');
    this.props.navigation.navigate('Zad3');
  }


  _showZad7 = () => {
    console.log('Navigating to Zad7');
    this.props.navigation.navigate('Zad7');
  }

  _showMoreApp = () => {
    console.log('Navigating to Other');
    this.props.navigation.navigate('Other');
  };

  _profileScreen = async () => {
    console.log('Navigating to Profile');
    this.props.navigation.navigate('Profile');
  };

}

// Some other screen for testing
class OtherScreen extends Component {
  static navigationOptions = {
    title: 'About',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ textAlign:'center'}}>Aplikacja scalająca wszystkie projekty (elementy projektów) z zajęć z programowania urządzeń mobilnych.{'\n\n'}
        AllInOne by Kevin Springer
        </Text>
        <StatusBar barStyle="default" />
      </View>
    );
  }

  componentWillUnmount(){
    console.log('Navigating back to Home');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: 'lightblue',
  },
  container_row1: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'powderblue',
  },
  container_row2: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'skyblue',
  },
  container_row3: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'steelblue',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  text: {
    textAlign: 'center',
  },
});



const AppStack = createStackNavigator({
  Home: HomeScreen,
  Zad1: FlexDimensionsBasics,
  Zad2: Klikacz,
  Zad3: RemoteGallery,
  Zad7: Ball,
  Other: OtherScreen,
  Profile: SignInScreen ,
});


export default createAppContainer(createSwitchNavigator(
  {
    App: AppStack,
  },
  {
    initialRouteName: 'App',
  }
));
