﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System;
using System.Globalization;
using Android.Support.V4.Content;
using Android.Support.V4.App;

namespace Intencje
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private Button przycisk;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            przycisk = FindViewById<Button>(Resource.Id.przycisk);
            przycisk.Click += przycisk_Click;
        }

        private void przycisk_Click(object sender, System.EventArgs e)
        {
            /*
            Intent i = new Intent(this, typeof(InnaAktywnosc));
            //Finish();
            StartActivity(i);
            */

            /*
            Intent i = new Intent(Intent.ActionView);
            i.SetData(Android.Net.Uri.Parse("http://www.umk.pl"));
            StartActivity(i);
            */

            /*
            Intent i = new Intent(Intent.ActionWebSearch);
            i.PutExtra(SearchManager.Query, "UMK");
            StartActivity(i);
            */

            /*
            IFormatProvider formatProvider = CultureInfo.InvariantCulture;
            double latitude = 53.017100;
            double longitude = 18.602889;
            double zoom = 15; //1-23
            string uri = "geo:" + latitude.ToString(formatProvider) + "," + longitude.ToString(formatProvider) + "?z=" + zoom.ToString(formatProvider);
            Intent i = new Intent(Intent.ActionView, Android.Net.Uri.Parse(uri));
            StartActivity(i);
            */

            string numerTelefonu = "+48 123 456 789";
            Intent i = new Intent(Intent.ActionCall);
            i.SetData(Android.Net.Uri.Parse("tel:" + numerTelefonu));
            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.CallPhone) !=
                Android.Content.PM.Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.CallPhone }, 0);
            }
            else
            {
                StartActivity(i);
            }

        }
    }
}