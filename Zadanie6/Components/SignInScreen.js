/* eslint-disable no-alert */
/* eslint-disable keyword-spacing */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App with Firebase
 * https://github.com/invertase/react-native-firebase
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import AsyncStorage from '@react-native-community/async-storage';

export default class SignInScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
    };
    this.user = null;
  }

  componentDidMount(){

    // initial configuration
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId: '139234108745-2ccvi2rrbund3n03v3dp8va50fk3cmna.apps.googleusercontent.com',
    });
    this.IsSignedIn();
  }

  componentWillUnmount(){
    console.log('Leaving SignIn/Profile Screen');
    //alert('Leaving SignIn/Profile Screen');
  }

  IsSignedIn = async() => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if(isSignedIn){
      console.log('Is already/still logged in!');
      this.GetCurrentUserInfo();
    }
    else{
      console.log('Please Login');
    }
    this.setState({gettingLoginStatus : false});
  };

  GetCurrentUserInfo = async() => {
    try{
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({userInfo : userInfo});
      console.log("Name: " + userInfo.user.name);
      const userInfoJSON = JSON.stringify(userInfo);
      await AsyncStorage.setItem('userInfo', userInfoJSON);
    }
    catch(error){
      console.log(' Error Message: ', error.message);
      if(error.code === statusCodes.SIGN_IN_REQUIRED){
        console.log('User has not signet in yet');
      }
      else{
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };

  SignIn = async() => {
    try{
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      this.user = userInfo;
      this.setState({ userInfo: userInfo});
      console.log("Name: " + userInfo.user.name);
      const userInfoJSON = JSON.stringify(userInfo);
      await AsyncStorage.setItem('userInfo', userInfoJSON);
    }
    catch(error){
      console.log(' Error Message: ', error.message);
      if(error.code === statusCodes.SIGN_IN_CANCELLED){
        console.log('Signing in has been cancelled');
      }
      else if(error.code === statusCodes.IN_PROGRESS){
        console.log('Signing in');
      }
      else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
        console.log('Play services not available');
      }
      else{
        console.log('Some other error happened');
      }
    }
  };

  SignOut = async() => {
    // Remove user session from device
    try{
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null}); // Remove the user form app's state
      await AsyncStorage.removeItem('userInfo');
    }
    catch(error){
      console.error(error);
    }
  }

  render(){
    // returning loader untill we check for already signed in user
    if(this.state.gettingLoginStatus){
      return(
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    else{
      if(this.state.userInfo != null){
        // Showing user details
        return(
          <View style={styles.container}>
            <Image source={{ uri: this.state.userInfo.user.photo }} style={styles.imageStyle} />
            <Text style={styles.text}>Name: {this.state.userInfo.user.name} {' '}</Text>
            <Text style={styles.text}>Email: {this.state.userInfo.user.email}</Text>
            <TouchableOpacity style={styles.button} onPress={this.SignOut}>
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>
        );
      }
      else{
        // for login showing signin button
        return(
          <View style={styles.container}>
            <GoogleSigninButton style={{ width: 312, height: 48}} size={GoogleSigninButton.Size.Wide} color={GoogleSigninButton.Color.Light} onPress={this.SignIn} />
          </View>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 30,
  },
});
