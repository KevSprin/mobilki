/* eslint-disable no-alert */
/* eslint-disable keyword-spacing */
/* eslint-disable prettier/prettier */
/**
 * Sample React Native App with Firebase
 * https://github.com/invertase/react-native-firebase
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, Button, StatusBar, Text } from 'react-native';
import SignInScreen, { } from './Components/SignInScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AsyncStorage from '@react-native-community/async-storage';

// Home screen of the app
class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      userInfo : null,
      isLoggedIn : false,
      prev: null,
    };
  }

  static navigationOptions = {
    title: 'Zadanie 6 : Firebase!',
  };

  render() {
    if(this.state.isLoggedIn === true){
      return (
        <View style={styles.container}>
          <Button title="Show me more of the app" onPress={this._showMoreApp}/>
          <Button title="Profile" onPress={this._profileScreen} />
        </View>
      );
    }
    else{
      return (
        <View style={styles.container}>
          <Button title="Sign in!" onPress={this._profileScreen} />
        </View>
      );
    }
  }

  componentDidMount(){
    console.log('Trying to retrieve data..');
    this.retrieveData();
  }

  componentDidUpdate(){
    this.retrieveData();
  }

  retrieveData = async() => {
    try{
      const info = await AsyncStorage.getItem('userInfo');
      if(this.state.prev !== info) {
        this.setState({userInfo : info});
        if(this.state.isLoggedIn === false && info !== null){
          console.log('Data retrieved!');
          this.setState({isLoggedIn : true});
        }
        else if(info === null && this.state.isLoggedIn === true && this.state.userInfo === null){
          console.log('No data retrieved or logged off');
          this.setState({isLoggedIn : false});
        }
      }
      this.setState({prev: info});
    }catch(error){
      console.log(' Error Message: ', error.message);
    }
  }

  _showMoreApp = () => {
    console.log('Navigating to Other');
    this.props.navigation.navigate('Other');
  };

  _profileScreen = async () => {
    console.log('Navigating to Profile');
    this.props.navigation.navigate('Profile');
  };

}

// Some other screen for testing
class OtherScreen extends Component {
  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Here will be some stuff later on</Text>
        <StatusBar barStyle="default" />
      </View>
    );
  }

  componentWillUnmount(){
    console.log('Navigating back to Home');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen, Profile: SignInScreen });

export default createAppContainer(createSwitchNavigator(
  {
    App: AppStack,
  },
  {
    initialRouteName: 'App',
  }
));
