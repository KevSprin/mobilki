/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View} from 'react-native';
import Greeting from './components/Greeting';
import RemoteGallery from './components/RemoteGallery';

export default class Zadanie3 extends Component {
  constructor(props) {
    super(props);
    this.state = {clicks: 0, ViewColor: 'skyblue', LastViewColor: 'steelblue'};
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'stretch',
        }}>
        <View style={{flex: 1}}>
          <Greeting name="Zdalna Galeria" backgroundColor="lightblue" />
          <RemoteGallery />
        </View>
      </View>
    );
  }
}
