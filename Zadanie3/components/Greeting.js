/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View} from 'react-native';

export default function Greeting(props) {
  return (
    <View
      style={{
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: props.backgroundColor,
      }}>
      <Text style={{fontSize: 24}}>{props.name}!</Text>
    </View>
  );
}
