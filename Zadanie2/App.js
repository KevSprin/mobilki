import React, { Component } from 'react';
import { Text, TextInput, View, Button, Image, Alert, FlatList, StyleSheet, TouchableOpacity, log  } from 'react-native';


const ListItems = [
  { key: 'Niebieski', value: 'blue'},
  { key: 'Zielony', value: 'green'},
  { key: 'Czerwony', value: 'red'},
  { key: 'Biały', value: 'white'},
  { key: 'Czarny', value: 'black'},
  { key: 'Żółty', value: 'yellow'},
  { key: 'Jasnoniebieski', value: 'lightblue'},
  { key: 'Metalicznyniebieski', value: 'steelblue'},
  { key: 'Niebobiebieski', value: 'skyblue'},
  { key: 'Pomarańczowy', value: 'orange'},
];
    
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'powderblue',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 12,
  },
});

export default class Zadanie2 extends Component {
  constructor(props){
    super(props);
    this.state = { clicks: 0, ViewColor: 'skyblue', LastViewColor: 'steelblue' };
  }
  changeColor(){
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    this.setState(previousState => ({ViewColor : 'rgb('+r.toString()+','+g.toString()+','+b.toString()+')'}));
  }

  changeWithSpecificColor(newColor){
    this.setState(previousState => ({LastViewColor : newColor}));
  }

  resetColor(){
    this.setState(previousState => ({ViewColor: 'white'}));
  }

  render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <View style={{flex: 1, backgroundColor: 'lightblue', justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{ fontSize: 18}}>Klikacz</Text>
        </View>
        <View style={{flex: 1, backgroundColor: 'powderblue', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row'}}>
          <View style={{flex: 2, padding:20}}>
            <Button title='+1' onPress={() => this.setState(previousState => ({clicks : previousState.clicks + 1}))} />           
          </View>
          <View style={{flex: 2, padding:20}}>
            <Button title='-1' onPress={() => this.setState(previousState => ({clicks : previousState.clicks - 1}))} />
          </View>
          <View style={{flex: 2, padding:10}}>
            <Text>Clicks: {this.state.clicks}</Text>
          </View>
        </View>
        <View title='Test' style={{flex: 1, backgroundColor: this.state.ViewColor, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity onPress={() => this.changeColor()} onLongPress={() => this.resetColor()}>
            <Text >Click Me For Color Change!</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, backgroundColor: this.state.LastViewColor}}>
          <FlatList
              data={ListItems}
              renderItem={({ item }) => <TouchableOpacity  onPress={() => { this.changeWithSpecificColor(item.value) }} style={styles.item}>
                                          <Text style={styles.title}>{item.key}</Text>
                                        </TouchableOpacity>}
              keyExtractor={item => item.key}
            />
        </View>
      </View>
    );
  }
}
