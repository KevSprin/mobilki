﻿using Android.App;
using Android.OS;
using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using System;

using Java.IO;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Java.Nio;
using Android.Hardware;
using Android.Runtime;

namespace OpenGLES
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private GLSurfaceView glView;
        private OpenGLRenderer renderer;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            glView = new GLSurfaceView(this);
            renderer = new OpenGLRenderer(ApplicationContext, true);
            glView.SetRenderer(renderer);
            SetContentView(glView);
        }

        protected override void OnPause()
        {
            base.OnPause();
            glView.OnPause();
        }

        protected override void OnResume()
        {
            base.OnResume();
            glView.OnResume();
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e) 
        {
            switch (keyCode)
            {
                case Keycode.W:
                case Keycode.DpadUp:
                    renderer.katX -= 1;
                    break;
                case Keycode.S:
                case Keycode.DpadDown:
                    renderer.katX += 1;
                    break;
                case Keycode.A:
                case Keycode.DpadLeft:
                    renderer.katY -= 1;
                    break;
                case Keycode.D:
                case Keycode.DpadRight:
                    renderer.katY += 1;
                    break;
            }
            return base.OnKeyDown(keyCode, e);
        }

        private bool poprzednie = false;

        private float poprzednieX = 0;

        private float poprzednieY = 0;

        private const float skalowanieZmianyKataDotyk = 0.002f;

        private float skalowanieX;
        
        private float skalowanieY;

        public override bool OnTouchEvent(MotionEvent e)
        {
            var akcja = e.Action;
            switch (akcja)
            {
                case MotionEventActions.Down:
                case MotionEventActions.Up:
                    poprzednie = false;
                    break;
                case MotionEventActions.Move:
                    float X = e.GetX();
                    float Y = e.GetY();
                    if (poprzednie)
                    {
                        float dX = X - poprzednieX;
                        float dY = Y - poprzednieY;
                        renderer.katX += skalowanieX * dY; // zamiana współrzędnych, bo oś/kierunek
                        renderer.katY += skalowanieY * dX;
                    }
                    else
                    {
                        poprzednie = true;
                        Display wyswietlacz = WindowManager.DefaultDisplay;
                        skalowanieX = wyswietlacz.Width / 2.0f * skalowanieZmianyKataDotyk;
                        skalowanieY = wyswietlacz.Height / 2.0f * skalowanieZmianyKataDotyk;
                    }
                    poprzednieX = X;
                    poprzednieY = Y;
                    break;
            }
            return base.OnTouchEvent(e);
        }
    }

    public class OpenGLRenderer : Java.Lang.Object, GLSurfaceView.IRenderer, ISensorEventListener
    {
        private Context context;
        public OpenGLRenderer(Context context, bool tesktury)
        {
            this.context = context;
            this.teksturowanie = tesktury;
        }

        private const float a = 1f;

        private float[] tablicaWerteksow =
        {
		    //tylnia
		    a,-a,-a,
            -a,-a,-a,
            a,a,-a,
            -a,a,-a,
		    //przednia
		    -a,-a,a,
            a,-a,a,
            -a,a,a,
            a,a,a,				

		    //prawa
		    a,-a,a,
            a,-a,-a,
            a,a,a,
            a,a,-a,
		    //lewa
		    -a,-a,-a,
            -a,-a,a,
            -a,a,-a,
            -a,a,a,
		
		    //gorna
		    -a,a,a,
            a,a,a,
            -a,a,-a,
            a,a,-a,
		    //dolna
		    -a,-a,-a,
            a,-a,-a,
            -a,-a,a,
            a,-a,a
        };

        private bool teksturowanie = true;

	    private float[] texCoords =
        { 
		    //tylnia
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
		    //przednia
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
		    //prawa
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
		    //lewa
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
		    //gorna
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
		    //dolna
		    0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f
        };

        private FloatBuffer buforWerteksow_WspTeksturowania;

        private float[,] kolory = new float[,]
        {
		    {1f,0f,0f,1f}, //tylnia
		    {1f,0f,0f,1f}, //przednia
		    {0f,1f,0f,1f}, //prawa
		    {0f,1f,0f,1f}, //lewa
		    {0f,0f,1f,1f}, //gorna
		    {0f,0f,1f,1f} //dolna
	    };

        private float[,] normalne = new float[,]
        {
		    {0f,0f,-1f}, //tylnia
		    {0f,0f,1f}, //przednia
		    {1f,0f,0f}, //prawa
		    {-1f,0f,0f}, //lewa
		    {0f,1f,0f}, //gorna
		    {0f,-1f,0f} //dolna
	    };

        public float katX = 0;
        
        public float katY = 0;

        private float[] m = new float[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

        public void OnSurfaceChanged(IGL10 gl, int width, int height)
        {
            if (height == 0) height = 1;
            UstawienieSceny(gl, width, height);
        }

        public void OnSurfaceCreated(IGL10 gl, Javax.Microedition.Khronos.Egl.EGLConfig config)
        {
            gl.GlShadeModel(GL10.GlSmooth);
            //ustawienia testu glebii (takie, jak domylne)
            gl.GlClearDepthf(1.0f);
            gl.GlEnable(GL10.GlDepthTest);
            gl.GlDepthFunc(GL10.GlLequal);
            gl.GlHint(GL10.GlPerspectiveCorrectionHint, GL10.GlNicest);
            gl.GlDisable(GL10.GlDither);

            InicjujBuforWerteksow();

            Oswietlenie(gl);

            if (teksturowanie)
            {
                LoadTexture(gl, context);
                gl.GlEnable(GL10.GlTexture2d);
            }
            else gl.GlDisable(GL10.GlTexture2d);

            //czujniki
            if (sensorManager == null)
            {
                sensorManager = context.GetSystemService(Context.SensorService).JavaCast<SensorManager>();
                orientacja = sensorManager.GetDefaultSensor(SensorType.Orientation);
                sensorManager.RegisterListener(this, orientacja, SensorDelay.Game);
            }

        }

        private void UstawienieSceny(IGL10 gl, int szer, int wys)
        {
            gl.GlViewport(0, 0, szer, wys);
            gl.GlMatrixMode(GL10.GlProjection);
            gl.GlLoadIdentity();
            GLU.GluPerspective(gl, 45.0f, (float)szer/ (float)wys, 0.1f, 100.0f);
            gl.GlMatrixMode(GL10.GlModelview);
            gl.GlLoadIdentity();
            GLU.GluLookAt(gl, 0, 0, 7.5f, 0, 0, 0, 0, 1, 0);	
        }

        private FloatBuffer buforWerteksow_Polozenia;
        private void InicjujBuforWerteksow()
        {
            ByteBuffer vbb = ByteBuffer.AllocateDirect(tablicaWerteksow.Length * 4); //float = 4 bajty
            vbb.Order(ByteOrder.NativeOrder());
            buforWerteksow_Polozenia = vbb.AsFloatBuffer(); //konwersja z bajtów do float
            buforWerteksow_Polozenia.Put(tablicaWerteksow); //kopiowanie danych do bufora
            buforWerteksow_Polozenia.Position(0);           //rewind

            if (teksturowanie)
            {
                ByteBuffer tbb = ByteBuffer.AllocateDirect(texCoords.Length * 4);
                tbb.Order(ByteOrder.NativeOrder());
                buforWerteksow_WspTeksturowania = tbb.AsFloatBuffer();
                buforWerteksow_WspTeksturowania.Put(texCoords);
                buforWerteksow_WspTeksturowania.Position(0);
            }
        }

        private void RysujSzescian(IGL10 gl, float krawedz, bool kolor)
        {
            gl.GlFrontFace(GL10.GlCcw);    //przednie sciany wskazane przez nawijanie przeciwne do ruchu wskazówek zegara
            gl.GlEnable(GL10.GlCullFaceCapability); //usuwanie tylnich powierzchni
            gl.GlCullFace(GL10.GlBack);

            if (krawedz != 1.0f) gl.GlPushMatrix(); //zapamietaj macierz model-widok (wloz na stos macierzy)		

            gl.GlEnableClientState(GL10.GlVertexArray);
            gl.GlVertexPointer(3, GL10.GlFloat, 0, buforWerteksow_Polozenia);

            if (teksturowanie)
            {
                gl.GlEnableClientState(GL10.GlTextureCoordArray);
                gl.GlTexCoordPointer(2, GL10.GlFloat, 0, buforWerteksow_WspTeksturowania);
            }

            if (!kolor) gl.GlColor4f(1f, 1f, 1f, 1f);

            for (int i = 0; i < 6; ++i)
            {
                gl.GlNormal3f(normalne[i,0], normalne[i,1], normalne[i,2]); // Dodajemy tę linię
                if (kolor) gl.GlColor4f(kolory[i,0], kolory[i,1], kolory[i,2], kolory[i,3]);
                gl.GlDrawArrays(GL10.GlTriangleStrip, i * 4, 4);
            }

            gl.GlDisableClientState(GL10.GlTextureCoordArray);
            gl.GlDisableClientState(GL10.GlVertexArray);
            gl.GlDisable(GL10.GlCullFaceCapability);
        }

        public void OnDrawFrame(IGL10 gl)
        {
            gl.GlClear(GL10.GlColorBufferBit | GL10.GlDepthBufferBit);

            //gl.GlRotatef(1, 1, 3, 0);
            gl.GlRotatef(katY, 0, 1, 0);
            gl.GlRotatef(katX, 1, 0, 0);

            float[] yx = new float[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
            Android.Opengl.Matrix.RotateM(yx, 0, katY, 0, 1, 0);
            Android.Opengl.Matrix.RotateM(yx, 0, katX, 1, 0, 0);
            //Android.Opengl.Matrix.MultiplyMM(m, 0, m, 0, yx, 0); //osie lokalne
            Android.Opengl.Matrix.MultiplyMM(m, 0, yx, 0, m, 0); //osie sceny


            //bez tego klawisze wplywaja na przyspieszenie
            katX = 0;
            katY = 0;

            gl.GlPushMatrix(); //zapamietaj macierz model-widok (wloz na stos macierzy)		
            RysujSzescian(gl, 1.0f, false);
            gl.GlPopMatrix(); //zdejmij ze stosu macierzy = odtworz zapamietany stan
        }

        private void Oswietlenie(IGL10 gl)
        {
            gl.GlEnable(GL10.GlLighting); //wlaczenie systemu oswietlania

            //nie ma color-material, wiec kolory w werteksach sa ignorowane (tylko tekstury)

            //źródła światła
            MlecznaZarowka(gl, GL10.GlLight1);
            Reflektor(gl, GL10.GlLight2);
        }

        private void MlecznaZarowka(IGL10 gl, int zrodloSwiatla)
        {
            float[] kolor1_rozproszone = { 0.5f, 0.5f, 0.5f, 1.0f };
            gl.GlLightfv(zrodloSwiatla, GL10.GlDiffuse, kolor1_rozproszone, 0);
            gl.GlEnable(zrodloSwiatla);
        }

        private void Reflektor(IGL10 gl, int zrodloSwiatla)
        {
            float[] kolor_rozproszone = { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] kolor_reflektora = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] pozycja = { 0.0f, -10.0f, 10.0f, 1.0f };
            float szerokosc_wiazki = 60.0f; //w stopniach
            gl.GlLightfv(zrodloSwiatla, GL10.GlPosition, pozycja, 0);
            gl.GlLightfv(zrodloSwiatla, GL10.GlDiffuse, kolor_rozproszone, 0);
            gl.GlLightfv(zrodloSwiatla, GL10.GlSpecular, kolor_reflektora, 0);
            gl.GlLightf(zrodloSwiatla, GL10.GlSpotCutoff, szerokosc_wiazki);
            gl.GlEnable(zrodloSwiatla);
        }

        int[] textureIDs = new int[1];   //tablica identyfikatorów

        //Uwaga! Oba rozmiary tekstury powinny być potęgami dwójki np. 256x256	
        public void LoadTexture(IGL10 gl, Context context)
        {
            gl.GlGenTextures(1, textureIDs, 0); //twórz tablicę 1D na tekstury

            gl.GlBindTexture(GL10.GlTexture2d, textureIDs[0]);   //Wiązanie tekstur z identyfikatorami z tablicy

            gl.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureMinFilter, GL10.GlNearest); //ustawienie filtrów tekstur
            gl.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureMagFilter, GL10.GlLinear);

            //wczytywanie obrazu z res\drawable-hdpi\tejstura.png
            System.IO.Stream istream = context.Resources.OpenRawResource(Resource.Drawable.tekstura);
            Bitmap bitmap;
            try
            {
                //dekoduj do obrazu
                bitmap = BitmapFactory.DecodeStream(istream);
            }
            finally
            {
                try
                {
                    istream.Close();
                }
                catch (IOException e)
                { }
            }

            //buduj tejsturę z odczytanej bitmapy
            GLUtils.TexImage2D(GL10.GlTexture2d, 0, bitmap, 0);
            bitmap.Recycle();
        }

        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {
        }

        private SensorManager sensorManager = null;

        Sensor orientacja = null;

        private float oz_azymut = 0; //stopnie

        private float ox_pochylenie = 0;

        private float oy_nachylenie = 0;

        private float o_dlugosc = 0;

        private bool o_0 = true;

        private float ox_0 = 0;

        private float oy_0 = 0;

        private float skalowaniePrzechylenia = 1;

        private long czas;

        public void OnSensorChanged(SensorEvent e)
        {
            if(e.Sensor.Type == SensorType.Orientation)
            {
                oz_azymut = e.Values[0];
                ox_pochylenie = e.Values[1];
                oy_nachylenie = e.Values[2];

                if (o_0)
                {
                    czas = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    ox_0 = ox_pochylenie;
                    oy_0 = oy_nachylenie;
                    o_0 = false;
                }

                ox_pochylenie -= ox_0;
                oy_nachylenie -= oy_0;

                float prog = 1; //stopnie
                if (Math.Abs(ox_pochylenie) < prog) ox_pochylenie = 0;
                if (Math.Abs(oy_nachylenie) < prog) oy_nachylenie = 0;

                katX -= skalowaniePrzechylenia * ox_pochylenie;
                katY -= skalowaniePrzechylenia * oy_nachylenie;

                long dt = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - czas;
                czas = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                if(dt > 0)
                {
                    katX /= dt;
                    katY /= dt;
                }
            }
        }
    }
}