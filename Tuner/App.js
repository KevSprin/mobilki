/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Text, PermissionsAndroid} from 'react-native';
import Tuner from './components/Tuner';

async function requestAudioRecordingPermission(){
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      {
        title: 'Tuner App Audio Recording Permission',
        message: 'Tuner app needs access to your microphone to record soundwaves',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED){
      console.log('Permission Granted!');
      return true;
    }
    else {
      console.log('Permission Denied!');
      return false;
    }
  }
  catch (err){
    console.warn(err);
    return false;
  }
}

export default class App extends Component {
  state = {
    name: null,
    value: null,
    cents: null,
    octave: null,
    frequency: null,
    audioPermission: null,
  };



  /**<Text>Tuner!</Text>
        <Text>Pitch : {this.state.name}</Text>
        <Text>Wave : {this.state.wavelength}</Text>
        <Text>Cents : {this.state.cents}</Text>
        <Text>Octave : {this.state.octave}</Text>
        <Text>Frequency : {this.state.frequency}</Text> */
  render() {
    if (this.state.audioPermission){
      let neg;
      let pos;
      let color = "yellow";
      let text = "Test";
      let x = Math.abs(this.state.cents);
      let valueN = Math.trunc( this.state.cents );
      let valueP = Math.trunc( this.state.cents );
      if (this.state.cents < 0){
        neg = Math.abs(this.state.cents) * 4;
        pos = 0;
        valueP = 0;
      }
      else {
        neg = 0;
        pos = this.state.cents * 4;
        valueN = 0;
      }
      if (x < 10){
        color = "yellowgreen";
        text = "Good! If you can try to get between -5 and +5 cents";
      }
      else if (x > 10){
        color = "gold";
        text = "Not enough!" + "\n" + " Keep the pitch between -10 and +10 cents to tune your guitar properly";
      }
      return (
        <View style={{flex: 1, flexDirection: "column", }}>
          <View style={{flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "lightblue"}}>
            <Text style={{fontSize: 40}}>Chromatic Tuner</Text>
          </View>

          <View style={{flex: 2, flexDirection: "column", alignItems: "center", justifyContent: "center", backgroundColor: "skyblue"}}>
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", flexDirection: "row"}}>
              <Text style={{fontSize: 40}}>Pitch : {this.state.name} </Text><Text style={{fontSize: 25}}>{this.state.octave}</Text>
            </View>
            <View>
              <Text style={{fontSize: 30}}>Frequency : {this.state.wavelength}Hz</Text>
            </View>
            <View style={{flex: 1, justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
              <Text style={{textAlign: "center", fontSize: 20}}>{text}</Text>
            </View>
          </View>
          <View style={{flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "lightslategrey"}}>
            <Text style={{fontSize: 25}}>Cents : {Math.trunc(x)}{"\n"}</Text>
            <View style={{ flexDirection: "row"}}>
              <View style={{ flex: 1, flexDirection: "column"}}>
                <View style={{ flexDirection: "row-reverse"}}>
                  <View style={{ backgroundColor: color, width: neg, height: 25, borderRadius: 10 }} />
                </View>
                <View style={{ alignItems: "center", justifyContent: "center"}}>
                  <Text style={{fontSize: 20 , fontWeight: "bold"}}>{valueN}</Text>
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "column"}}>
                <View style={{ flex: 1}}>
                  <View style={{ backgroundColor: color, width: pos, height: 25, borderRadius: 10 }} />
                </View>
                <View style={{ alignItems: "center", justifyContent: "center"}}>
                  <Text style={{fontSize: 20 , fontWeight: "bold"}}>{valueP}</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    }
    else {
      return (
        <View style={{flex: 1, flexDirection: "column", justifyContent: "center"}}>
          <Text>This app needs permission for audio recording to work properly!</Text>
        </View>
      );
    }
  }

  async componentDidMount() {
    const request = await requestAudioRecordingPermission();
    if (request){
      this.setState({audioPermission: request});
      const tuner = new Tuner();
      tuner.startTuner();
      tuner.detectedPitch = pitch => {
        this.setState({
          name: pitch.name,
          wavelength: pitch.wavelength,
          cents: pitch.cents,
          octave: pitch.octave,
          frequency: pitch.freq,
        });
      };
    }
  }
}
