/* eslint-disable radix */
//import Recording from 'react-native-recording';
import RNTunerRecorder from 'react-native-tuner-recorder';
import PitchFinder from 'pitchfinder';
import {NativeEventEmitter} from 'react-native';

const eventEmitter = new NativeEventEmitter(RNTunerRecorder);
export default class Tuner {
  pitches = ['C', 'C♯', 'D', 'D♯', 'E', 'F', 'F♯', 'G', 'G♯', 'A', 'A♯', 'B'];
  A4 = 440;
  semitone = 69;

  getCallback = message => {
    console.log(message);
  };

  constructor(sampleRate = 22050, bufferSize = 2048) {
    this.sampleRate = sampleRate;
    this.bufferSize = bufferSize;
    this.pitchFinder = new PitchFinder.YIN({sampleRate: this.sampleRate});
  }

  startTuner() {
    RNTunerRecorder.init(
      {
        sampleRate: this.sampleRate,
        bufferSize: this.bufferSize,
      },
      this.getCallback,
    );
    RNTunerRecorder.start(this.getCallback);
    eventEmitter.addListener('recording', capture => {
      const freq = this.pitchFinder(capture);
      const wavelength = this.getPitch(freq);
      const name = this.pitches[wavelength % 12];
      const cents = this.getCents(freq, wavelength);
      const octave = parseInt(wavelength / 12) - 1;

      if (freq && this.detectedPitch) {
        this.detectedPitch({
          name: name,
          wavelength: wavelength,
          cents: cents,
          octave: octave,
          freq: freq,
        });
      }
    });
  }

  getCallback = message => {
    console.log(message);
  };

  getPitch(freq) {
    const pitch = 12 * (Math.log(freq / this.A4) / Math.log(2));
    return Math.round(pitch) + this.semitone;
  }

  getStandardFrequency(pitch) {
    return this.A4 * Math.pow(2, (pitch - this.semitone) / 12);
  }

  getCents(freq, pitch) {
    const tmp =
      Math.floor(1200 * Math.log(freq / this.getStandardFrequency(pitch))) /
      Math.log(2);
    return tmp;
  }
}
