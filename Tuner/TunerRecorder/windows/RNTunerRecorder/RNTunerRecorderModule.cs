using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Tuner.Recorder.RNTunerRecorder
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNTunerRecorderModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNTunerRecorderModule"/>.
        /// </summary>
        internal RNTunerRecorderModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNTunerRecorder";
            }
        }
    }
}
