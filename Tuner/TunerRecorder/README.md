
# react-native-tuner-recorder

## Getting started

`$ npm install react-native-tuner-recorder --save`

### Mostly automatic installation

`$ react-native link react-native-tuner-recorder`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-tuner-recorder` and add `RNTunerRecorder.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNTunerRecorder.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNTunerRecorderPackage;` to the imports at the top of the file
  - Add `new RNTunerRecorderPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-tuner-recorder'
  	project(':react-native-tuner-recorder').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-tuner-recorder/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-tuner-recorder')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNTunerRecorder.sln` in `node_modules/react-native-tuner-recorder/windows/RNTunerRecorder.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Tuner.Recorder.RNTunerRecorder;` to the usings at the top of the file
  - Add `new RNTunerRecorderPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNTunerRecorder from 'react-native-tuner-recorder';

// TODO: What to do with the module?
RNTunerRecorder;
```
  